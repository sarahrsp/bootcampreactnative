import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import AboutScreen from './Tugas/Tugas 13/AboutScreen';
import LoginScreen from './Tugas/Tugas 13/LoginScreen';

export default function App() {
  return (

    // <LoginScreen />
    <AboutScreen/>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
