import React from 'react'
import {Stylesheet, 
    Text, 
    View, 
    ScrollView, 
    TextInput, 
    Button} from 'react-native'

const AboutScreen = () => {
    return (
        <ScrollView> 
        <View style={styles.container}>
            <Text>Tentang Saya</Text>
            <fontAwesome5 name="user-circle"/>
            <Text>Sarah Panjaitan</Text>
            <Text>Tukang ketik</Text>
            <View style={styles.kotak}>
                <Text>Portofolio</Text>
                <View style={styles.kotakdalam}>
                    <View>
                    <fontAwesome5 name ="gitlab"/>
                    <Text>@sarahrsp</Text>
                    </View>
                    <View>
                    <fontAwesome5 name ="github"/>
                    <Text>@sarahrsp</Text>
                    </View>
                </View>
            </View>
            <View style={styles.kotak}>
                <Text>Hubungi saya</Text>
                <View style={styles.kotakdalamver}>
                    <View style={styles.kotakdalamverhub}>
                        <View>
                            <fontAwesome5 name="Instagram"/>
                            <Text>@sarahrsp</Text>
                        </View>

                    </View>
                </View>

            </View>
        </View>
        </ScrollView>
    )
}

export default AboutScreen

const styles = Stylesheet.create({})