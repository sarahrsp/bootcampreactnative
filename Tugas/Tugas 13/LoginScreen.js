import React from 'react'
import { Platform, 
    Image, 
    ScrollView, 
    Stylesheet, 
    Text, 
    View, 
    TextInput, 
    TouchableOpacity, 
    Button, 
    KeyboardAvoidingView } from 'react-native'

const LoginScreen = () => {
    return (
        <KeyboardAvoidingView
        behavior = {Platform.OS == "ios" ? "padding" : "height"}
        style={styles.container}
        >
        <ScrollView> 
            <View style={styles.containerView}>
                <Image source={require('../Tugas 13/asset/logo.png')}/>
                <Text styles={styles.logintext}>Login</Text>
                <View style={styles.forminput}>
                    <Text style={styles.input}>Username</Text>
                    <TextInput style={styles.input}/>
                </View>
                <View style={styles.forminput}>
                    <Text style={styles.input} secureTextEntry={true}>Password</Text>
                    <TextInput/>
                </View>
                <View style={styles.kotaklogin}>
                    <TouchableOpacity style={styles.btlogin}>
                        <Text style={styles.textbt}>Masuk</Text>
                    </TouchableOpacity>
                    <Text style={styles.autotext}>Atau</Text>
                    <TouchableOpacity style={styles.btreg}>
                        <Text style={styles.textbt}>Daftar</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </ScrollView>
        </KeyboardAvoidingView>
    )
}

export default LoginScreen

const styles = Stylesheet.create({
    container:{
        flex:1
    
    }, logintext:{
        fontsize: 24,
        marginTop:63,
        textAlign: 'center',
        color: '#003366',
        marginVertical: 20
    },
    formtext:{
        color: '#003366',
        fontsize: 20
    }, autotext:{
        fontsize: 20,
        color: 
    }, forminput:{
        marginHorizontal: 30,
        marginVertical: 5,
        alignContent: 'center',
        width: 294
    },
    input:{
        height: 40,
        borderColor: "#003366",
        padding: 10,
        borderWidth: 1
    }, btlogin:{
        alignItems: 'center',
        backgroundColor: "#3EC6FF",
        padding: 10,
        borderRadius: 16,
        marginHorizontal: 30,
        marginBottom: 10,
        width: 140,
    }
