import React from 'react';
import { createStackNavigator} from '@react-navigation/native/stack';
import { createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import { createDrawerNavigator} from '@react-navigation/drawer';
import { NavigationContainer} from '@react-navigation/native';

import login from './login';
import register from './register';
import home from './home';
import detailbooked from './detailbooked';
import payment from './payment';
import success from './success';

const Stack = createStackNavigator();
const Tab = createBottomNavigator();
conts Drawer = createDrawerNavigator();

const navigation =()=>{
    return (
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen name="LoginScreen" component={LoginScreen}/>
                <Stack.Screen name="Drawer" component={Drawer}/>
            </Stack.Navigator>
        </NavigationContainer>
    )
}
const Drawer = ()=>{
    return(
        <Drawer.Navigator>
            <Drawer.Screen name='register' component={register}/>
            <Drawer.Screen name='home' component={home}/>
        </Drawer.Navigator>
    )
}

const home=()=>{
    return(
        <Tab.Navigator>
             <Tab.Screen name='detailbooked' component={detailbooked}/>
             <Tab.Screen name='payment' component={payment}/>
             <Tab.Screen name='success' component={success}/>
        </Tab.Navigator>
    )
}

export default navigation;