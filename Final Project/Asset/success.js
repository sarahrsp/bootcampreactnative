import React from 'react'
import { Platform, 
    Image, 
    Stylesheet, 
    Text, 
    View, 
    TextInput, 
    TouchableOpacity, 
    Button, 
    KeyboardAvoidingView } from 'react-native'

const SuccessScreen = () => {
    return (
        <Text>Transaksi sebesar Rp 684.000 berhasil dicatat</Text>
    )
}

export default LoginScreen
